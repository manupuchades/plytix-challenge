# PLYTIX-CHALLENGE

Prueba técnica para Plytix.

## Description

La prueba trata de hacer un API en Flask o FastAPI (ambos son frameworks de python para desarrollar aplicaciones web). Como base de datos se utilizará MongodB.  

### Los endpoints serán los siguientes

- **GET /words/** Devuelve un listado de palabras ordenadas. Ojo, que el orden no es orden alfabético, sino el orden en el que están guardadas.

- **POST /words/** Añade una palabra. El payload será la palabra en sí y la posición.  
  
- **PATCH /words/*\<palabra>*** Obtiene "position" del payload y pone esa palabra en la posición indicada
  
- **GET /words/*\<palabra>*/anagrams** Devuelve los anagramas de una palabra. La palabra no tiene por qué formar parte del conjunto de palabras.
  
- **DELETE /words/*\<palabra>*** Borra la palabra  

### Ejemplo

>1. GET /words  
code: 200  
resp:
{data: [
"cosa",
"caso",
"paco",
"pepe",
"Málaga"]
}

>2. POST /words {"word": "calle", "position": 3}  
code: 201  
resp:
{"word": "calle", "position": 3}

>3. GET /words  
code: 200  
resp:
{data: [
"cosa",
"caso",
"calle",
"paco",
"pepe",
"Málaga"]
}

>4. PATCH /words/calle {"position": 5}  
code: 200  
resp:
{"word": "calle", "position": 5}

>5. GET /words  
code: 200  
resp:
{data: [
"cosa",
"caso",
"paco",
"pepe",
"calle",
"Málaga"]
}

>6. GET /words/asco/anagrams  
code: 200  
resp:
{data: [
"cosa",
"caso"]
}

>7. DELETE /words/calle  
code: 204

>8. GET /words  
code: 200  
resp:
{data: [
"cosa",
"caso",
"paco",
"pepe",
"Málaga"]
}

### ¿Qué se valora en la prueba?

- Resolución a los problemas de ordenación y anagramas
- Organización/ estructuración del código
- Cualquier otro extra que quieras añadir (docker/docker-compose/tests/...)

***

## Solution

### Structure of the application

The structure of the application is the following:

    plytix-challenge/
      api/
        __init__.py
        models/  
          __init__.py
          db.py
          words.py
        routes/
          __init__.py
          exception_handler.py
          words.py
        schemas/
          __init__.py
          word.py
        services/
          __init__.py
          word.py
        swagger_specs/
      test/
        __init__.py
        routes/
            __init__.py
            test_home.py
      .gitignore
      .gitlab-ci.yml
      app.py
      config.py
      Pipfile
      Pipfile.lock

All the application magic happens inside the API module (`/api`), there, we split the code into 4 main parts:

- The `models` are the data descriptors of our application related to the database model.

- The `routes` are the URIs to our application, where we define our resources and actions.

- The `schemas` are the definitions for inputs and outputs of our API, what parameters are allowed, what information we will output.

- The `services` are modules that define application logic and interact with the db layer.

## Installation

The app relies on a MongoDB database to store data. Please ensure you have an instance running on your machine.  
By default the application expects the database to be running at localhost, see the paramenters section bellow to customize it.

### Local

Pipenv is used to create a virtual environment which isolates the python packages you used in this project from other system python packages.

```sh
pipenv install
pipenv shell
```

Run the app

```sh
$ python app.py
 * Serving Flask app 'app' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
```

### Docker

Use the following commands to build the docker image & run it in a container.
By default the application expects a mongodb instance is available at localhost.

```sh
docker build . -t plytix-challenge:v1.0
docker run -p 5000:5000 plytix-challenge:v1.0
```

Using the following command both mongodb & flask app will be up and running:

```sh
docker-compose up
```

### Parameters

The following parameters can be customized in environment variables:  

- *APP_CONFIG* : {local, prod, docker} - local by default  
- *APP_PORT* : - 5000 by default  
- *APP_DEBUG*: {True, False} - True by default  
- *MONGODB_HOSTNAME* : - localhost by default  
- *MONGODB_DATABASE* : - words by default  

## Test

The code is covered by tests, to run the tests please execute.

```sh
pipenv run python -m unittest
```

Additionally a postman collection is used to test the API directly in Postman with the test scenario described in the challenge description.

## Documentation

Visit <http://localhost:5000/apidocs> for the swagger documentation

## Roadmap

This is the content of the featured release branches:  

- **main**  
  Contains the latest stable version of the code. No work is done directly into this branch.
- **feature/initial_setup**
  Basic project implementation, includes all REST Endpoints with in-memory data management.
- **feature/mongo_db**
  Includes mongodb for data storage.
- **feature/docker**
  Prepare the application to be container ready. Include Docker & Docker-Compose support.
- **feature/data_validation**
  Add input validation with marshmallow and test it with unittest.
- **feature/documentation**
  Add REST API documentation with flasgger.
- **feature/enhanced_anagrams**
  Improve performance in anagrams search

## Gitlab CI

Continous integration performed by Gitlab CI includes the following stages:

- `Static analysis` : Type checking using mypy & linting using flake8.
- `Test` : Unit testing.
- `Build` : Build & publish docker image into Gitlab registry.
- `Api Test` : Basic healthcheck.
- `Acceptance` : End to end suitcase using postman.
