FROM python:3.10.5-slim

WORKDIR /opt

# Install & use pipenv
COPY Pipfile Pipfile.lock ./
RUN python -m pip install --upgrade pip
RUN pip install pipenv && pipenv install --dev --system --deploy

COPY api api
COPY app.py config.py ./

# run-time configuration
ENV APP_CONFIG docker
ENV FLASK_APP=app

EXPOSE 5000
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]