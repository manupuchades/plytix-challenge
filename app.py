from flask import Flask
from flasgger import Swagger
from api.models.db import initialize_db
from api.models.db_setup import insert_initial_dataset
from api.routes.words import words_api
from api.routes.exception_handler import handler
from config import config


def create_app():
    app = Flask(__name__)
    app.config.from_object(config())
    initialize_db(app)
    insert_initial_dataset()
    Swagger(app)

    app.register_blueprint(words_api)
    app.register_blueprint(handler)

    return app


if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=config().APP_PORT, debug=config().APP_DEBUG)
