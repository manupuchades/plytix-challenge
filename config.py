import os


class Config:
    APP_DEBUG = os.environ.get("APP_DEBUG", True)
    APP_PORT = os.environ.get("APP_PORT", 5000)
    SWAGGER = {'title': 'Plytix challenge API Documentation', }


class LocalConfig(Config):
    FLASK_ENV = 'development'
    MONGODB_SETTINGS = {'host': 'mongodb://'
                        + os.environ.get('MONGODB_HOSTNAME', 'localhost')
                        + '/' + os.environ.get('MONGODB_DATABASE', 'words')}


class ProdConfig(Config):
    FLASK_ENV = 'production'
    MONGODB_SETTINGS = {'host': 'mongodb://'
                        + os.environ.get('MONGODB_HOSTNAME', 'localhost')
                        + '/' + os.environ.get('MONGODB_DATABASE', 'words')}


class DockerConfig(Config):
    FLASK_ENV = 'development'
    MONGODB_SETTINGS = {'host': 'mongodb://'
                        + os.environ.get('MONGODB_HOSTNAME',
                                         'host.docker.internal')
                        + '/' + os.environ.get('MONGODB_DATABASE', 'words')}


def config():
    return config_env[os.getenv('APP_CONFIG') or 'default']


config_env = {
    'local': LocalConfig,
    'prod': ProdConfig,
    'docker': DockerConfig,
    'default': LocalConfig
}
