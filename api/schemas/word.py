from marshmallow import Schema, fields, validate


class WordSchema(Schema):
    word = fields.String(
        validate=validate.Regexp("^[A-Za-zÀ-ÿ]+$"),
        required=True
    )

    position = fields.Integer(
        strict=True,
        required=True,
        validate=[validate.Range(min=1)]
    )
