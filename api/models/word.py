from .db import db


class Word(db.Document):
    word = db.StringField(required=True)
    sorted_word = db.StringField()
