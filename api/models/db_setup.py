from .word_list import WordList
from .word import Word

initial_dataset = {
    "data": ["cosa", "caso", "paco", "pepe", "Málaga"]
}


def insert_initial_dataset():
    word_list = WordList.getData()
    if not word_list:
        word_list = WordList()
        for item in initial_dataset["data"]:
            word_list["data"].append(item)
            Word(word=item, sorted_word=''.join(sorted(item))).save()
        word_list.save()
