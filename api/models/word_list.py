from .db import db


class WordList(db.Document):
    data = db.ListField()

    @staticmethod
    def getData():
        return WordList.objects.first()
