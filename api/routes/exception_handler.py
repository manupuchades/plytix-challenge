from marshmallow.exceptions import ValidationError
from werkzeug.exceptions import Conflict, NotFound, BadRequest
from flask import Blueprint
from flask import jsonify

handler = Blueprint('exception_handler', __name__)


def format_response(status, error, message):
    response = jsonify({'error': error, 'message': message})
    response.status_code = status
    return response


@handler.app_errorhandler(ValidationError)
def validation_error(e):
    return format_response(BadRequest.code, 'Bad Request', e.args[0])


@handler.app_errorhandler(Conflict)
def conflict_error(e):
    return format_response(Conflict.code, 'Conflict', e.description)


@handler.app_errorhandler(NotFound)
def notfound_error(e):
    return format_response(NotFound.code, 'Not Found', e.description)
