from flask import Blueprint, request, jsonify
from flasgger import swag_from
from api.schemas.word import WordSchema
from api.services.word import WordService

words_api = Blueprint('words', __name__)


@words_api.route('/words')
@swag_from('../swagger_specs/get_words.yml')
def get_words():
    return jsonify(WordService().getList()), 200


@words_api.route('/words', methods=['POST'])
@swag_from('../swagger_specs/post_words.yml')
def add_word():
    word_request = WordSchema().load(request.get_json())
    return jsonify(WordService().save(word_request)), 201


@words_api.route('/words/<word>', methods=['PATCH'])
@swag_from('../swagger_specs/patch_words.yml')
def move_word(word):
    position = int(request.get_json()['position'])
    word_request = WordSchema().load({'word': word, 'position': position})
    return jsonify(WordService().move_word(word_request)), 200


@words_api.route('/words/<word>/anagrams', methods=['GET'])
@swag_from('../swagger_specs/get_words_anagrams.yml')
def get_anagrams(word):
    word_request = WordSchema().load({'word': word}, partial=True)
    return jsonify(WordService.find_anagrams(word_request)), 200


@words_api.route('/words/<word>', methods=['DELETE'])
@swag_from('../swagger_specs/delete_words.yml')
def delete_word(word):
    word_request = WordSchema().load({'word': word}, partial=True)
    WordService.delete(word_request)
    return 'None', 204
