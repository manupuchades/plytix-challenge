from api.models.word_list import WordList
from api.models.word import Word
from werkzeug.exceptions import Conflict, NotFound


class WordService:

    @classmethod
    def getList(cls):
        return {"data": WordList.getData().data}

    @classmethod
    def save(cls, word_request):
        if Word.objects(word__iexact=word_request["word"]).first():
            raise Conflict(
                "A conflict happened while processing the request: "
                + word_request["word"] + " already present in the database.")

        Word(word=word_request["word"],
             sorted_word=''.join(sorted(word_request["word"]))).save()

        word_list = WordList.getData()
        word_list["data"].insert(
            word_request["position"] - 1, word_request["word"])
        word_list.save()

        return word_request

    @classmethod
    def delete(cls, word_request):
        word = Word.objects(word=word_request["word"]).first()
        if not word:
            raise NotFound(
                "The requested resource was not found: "
                + word_request["word"] + " not found.")

        word_list = WordList.getData()
        word_list["data"].remove(word_request["word"])
        word_list.save()
        word.delete()

    @classmethod
    def move_word(cls, word_request):
        if not Word.objects(word=word_request["word"]).first():
            raise NotFound(
                "The requested resource was not found: "
                + word_request["word"] + " not found.")

        word_list = WordList.getData()
        word_list["data"].remove(word_request["word"])
        word_list["data"].insert(
            word_request["position"] - 1, word_request["word"])
        word_list.save()
        return word_request

    @classmethod
    def find_anagrams(cls, word_request):
        sorted_word_request = ''.join(sorted(word_request["word"]))
        anagrams = {"data": []}

        for item in Word.objects(sorted_word__iexact=sorted_word_request):
            anagrams["data"].append(item.word)

        return anagrams
