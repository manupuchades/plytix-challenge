import unittest
import json

from app import create_app


class APITestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app().test_client()

    def test_get_words_is_200(self):
        get_response = self.app.get('/words')

        self.assertEqual(get_response.status_code, 200)
        self.assertEqual(get_response.get_json(),
                         {"data": ["cosa", "caso", "paco", "pepe", "Málaga"]})

    def test_post_words_is_201(self):
        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "calle", "position": 3}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 201)
        self.assertEqual(post_response.get_json(),
                         {"word": "calle", "position": 3})

        delete_response = self.app.delete('/words/calle')
        self.assertEqual(delete_response.status_code, 204)

    def test_post_words_conflict(self):
        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "test", "position": 3}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 201)
        self.assertEqual(post_response.get_json(),
                         {"word": "test", "position": 3})

        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "test", "position": 3}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 409)

        delete_response = self.app.delete('/words/test')
        self.assertEqual(delete_response.status_code, 204)

    def test_post_words_bad_request_missing_params(self):
        post_response = self.app.post(
            '/words',
            data=json.dumps({"w": "calle", "position": 3}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 400)

        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "calle", "p": 3}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 400)

    def test_post_words_bad_request_additional_params(self):
        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "calle", "position": 3, "foo": "foo"}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 400)

    def test_post_words_bad_request_incorrect_position(self):
        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "test", "position": "a"}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 400)

        post_response = self.app.post(
            '/words',
            data=json.dumps({"word": "test", "position": -1}),
            content_type='application/json')

        self.assertEqual(post_response.status_code, 400)

    def test_patch_words_not_found(self):
        patch_response = self.app.patch(
            '/words/missing',
            data=json.dumps({"position": "5"}),
            content_type='application/json')

        self.assertEqual(patch_response.status_code, 404)

    def test_delete_words_not_found(self):
        delete_response = self.app.delete(
            '/words/missing')

        self.assertEqual(delete_response.status_code, 404)
